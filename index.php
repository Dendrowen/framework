<?php

include 'core/config.php';

echo "<html>";
    echo "<head>";
        include 'core/head.php';
    echo "</head>";
    echo "<body>";

        include 'core/header.php';

        include "pages/" . $page . ".php";

        echo "<div class='errorcontainer'>";
            App::displayErrors();
        echo "</div>";

        include 'core/footer.php';

    echo "</body>";
echo "</html>";

DB::close();